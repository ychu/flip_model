# FLIP_Model

This is part of the FLIP project. It contains a work-in-progress exterior model of the RP FLIP vessel and is intended to be ideal for 3D printing.
Learn more about the RP FLIP at its wiki page: [RP FLIP](https://en.wikipedia.org/wiki/RP_FLIP)

## References

The FLIP project website: [FLIP](https://www.graphicocean.org/work-1/flip-book)

Photogrammetry tools: [Point cloud system](https://pointcloud.ucsd.edu/SIO/FLIP/FLIP_assembly.html)

Photos and measurements: [Google drive](https://drive.google.com/drive/folders/1nBR5JnxMaG-Y3WX7PWPOj5CvQZnw8Lm8)

## Screenshots & 3D Printing Samples
![Fig.1](/FLIP1.png)
![Fig.2](/FLIP2.png)
![Fig.3](/FLIP3.png)
![Fig.4](/3D_printing_test.JPG)
